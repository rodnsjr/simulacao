package escalonamento;
import java.util.ArrayList;
import java.util.Collections;

import principal.EntidadeTemporaria;


public class EscSRT extends Escalonamento{

	
	ArrayList<EntidadeTemporaria> saida = new ArrayList<EntidadeTemporaria>();
	ArrayList<EntidadeTemporaria> fila = new ArrayList<EntidadeTemporaria>();
	boolean atendendo = false;
	
	public EscSRT(ArrayList<EntidadeTemporaria> et) {
		super(et);
		this.taxaOciosidade = 0;
		this.tempoTotaldeExecucao = 0;
		this.tempoMedioEsperaEmFila = 0;
		this.calcula = new Ociosidade_TMEF();
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<EntidadeTemporaria> escalonar()
	{
			
		ArrayList<EntidadeTemporaria> temp = new ArrayList<EntidadeTemporaria>();
		boolean firstEntry = true;
		
		for (EntidadeTemporaria entidadeTemporaria : et) {
			if (firstEntry)
			{
				firstEntry = false;
				continue;
			}
			
			temp.add(entidadeTemporaria);
		}
		
		Collections.sort(temp, EntidadeTemporaria.SortByTC);		
		
		//temp.add(0, et.get(0));
		
		//gerar TEA, TSA, TEF
		EntidadeTemporaria emAtendimento = et.get(0);
		emAtendimento.setTea(emAtendimento.getTc());
		emAtendimento.setTsa(emAtendimento.getTc() + emAtendimento.getTa());
		emAtendimento.setTef();
		
		for (EntidadeTemporaria entidadeTemporaria : temp) {
			entidadeTemporaria.setTre(entidadeTemporaria.getTa());
		}
		
		fila = new ArrayList<EntidadeTemporaria>();
		
		//pegar o tempo de saida do emAtendimento
		//checar no array quais entidades tem o tempo de chegada menor que esse tempo de saida
		//colocar eles em fila, e escalonar por ordem de atendimento
		//atender o pr�ximo
		while (!temp.isEmpty())
		{
			
			EntidadeTemporaria tsaida = null;
			
			//chegou a entidade joga a em atendimento na fila, e atende essa
			if (temp.get(0).getTc() <= emAtendimento.getTsa())
			{
				tsaida = temp.remove(0);
				emAtendimento.setTsa(tsaida.getTc());
				
				int atende = emAtendimento.getTre() - (emAtendimento.getTsa() - emAtendimento.getTea());
				
				if (atende < 0)
					atende = 0;
				
				emAtendimento.setTre(atende);
				
				//j� mostra na saida as entradas da fila, e o tempo restante de execu��o
				enfila(emAtendimento.clone());
				
				//emAtendimento = temp.get(i);
				
				tsaida.setTea(emAtendimento.getTsa());			
				tsaida.setTsa(tsaida.getTea() + tsaida.getTre());
				tsaida.setTef();
				
				emAtendimento = tsaida;
			}
			//Caso haja entidades na fila
			else if (!fila.isEmpty())
			{
				//com tempo de chegada habil
				if (fila.get(0).getTc() <= emAtendimento.getTsa())
				{
					tsaida = fila.remove(0);
					//emAtendimento.setTsa(tsaida.getTea());

					int atende = emAtendimento.getTre() - (emAtendimento.getTsa() - emAtendimento.getTea());
					emAtendimento.setTsa(emAtendimento.getTea() + emAtendimento.getTre());
					if (atende < 0)
						atende = 0;
					
					emAtendimento.setTre(atende);
					
					//this.saida.add(emAtendimento);
					enfila(emAtendimento.clone());
					//emAtendimento = temp.get(i);
					
					tsaida.setTea(emAtendimento.getTsa());			
					tsaida.setTsa(tsaida.getTea() + tsaida.getTre());
					tsaida.setTef();
					//tsaida.setTre()
					
					emAtendimento = tsaida;
				}
			}
			else
			{
				tsaida = temp.remove(0);
				emAtendimento.setTsa(emAtendimento.getTea()+emAtendimento.getTa());				
				emAtendimento.setTre(0);
				
				this.saida.add(emAtendimento);
				
				tsaida.setTea(tsaida.getTc());
				tsaida.setTsa(tsaida.getTea()+tsaida.getTre());
				tsaida.setTef();
				
				this.taxaOciosidade = this.taxaOciosidade + (tsaida.getTea() - emAtendimento.getTsa());
				emAtendimento = tsaida;
			}
		}
		
		emAtendimento.setTre(0);
		this.saida.add(emAtendimento);
		
		//limpa o restante da fila... obs n�o tem ociosidade mais, pq s� t� limpando os que ficaram para tr�s
		for (int i = 0; i < fila.size(); i++)
		{
			EntidadeTemporaria tsaida = null;
			tsaida = fila.get(i);
			
			if (tsaida.getTc() <= emAtendimento.getTsa())
			{
				tsaida.setTea(emAtendimento.getTsa());
			}
			else
			{
				tsaida.setTea(tsaida.getTc());
				//this.taxaOciosidade = this.taxaOciosidade + (tsaida.getTea() - emAtendimento.getTsa());
			}
			
			tsaida.setTsa(tsaida.getTea() + tsaida.getTre());
			
			int atende = emAtendimento.getTre() - (emAtendimento.getTsa() - emAtendimento.getTc());
			
			if (atende < 0)
				atende = 0;
			
			tsaida.setTre(atende);
			tsaida.setTef();
			
			this.saida.add(tsaida);
			
			emAtendimento = tsaida;
		}
		
		this.tempoTotaldeExecucao = saida.get(saida.size() - 1).getTsa();
        this.taxaOciosidade = calcula.getTaxaOciosidade(taxaOciosidade, tempoTotaldeExecucao);
        this.tempoMedioEsperaEmFila = calcula.getMediaTef(saida);
		return saida;
		

	}
	
	private void enfila(EntidadeTemporaria entidadeTemporaria) {
		// TODO Auto-generated method stub
		saida.add(entidadeTemporaria.clone());

		//entidadeTemporaria.setTea(entidadeTemporaria.getTsa());
		if (entidadeTemporaria.getTre() > 0)
		{
			entidadeTemporaria.setTc(entidadeTemporaria.getTsa());
			entidadeTemporaria.setTa(entidadeTemporaria.getTre());
			fila.add(entidadeTemporaria);
		}
		
		if (fila.size() > 1)
			Collections.sort(fila, EntidadeTemporaria.SortByTRE);
	}

}