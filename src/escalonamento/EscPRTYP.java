package escalonamento;

import java.util.ArrayList;

import principal.EntidadeTemporaria;

public class EscPRTYP extends Escalonamento {
	

	public EscPRTYP(ArrayList<EntidadeTemporaria> et) {
		super(et);
		this.taxaOciosidade = 0;
		this.tempoTotaldeExecucao = 0;
		this.tempoMedioEsperaEmFila = 0;
		this.calcula = new Ociosidade_TMEF();
		// TODO Auto-generated constructor stub
	}

	@Override
	public ArrayList<EntidadeTemporaria> escalonar() {
		
		ArrayList <EntidadeTemporaria> etAux = new ArrayList<EntidadeTemporaria>();
	    ArrayList <EntidadeTemporaria> filaAux = new ArrayList<EntidadeTemporaria>();
	        
	       
	        for (int i = 0; i < this.et.size(); i++) {
	            filaAux.add(this.et.get(i));
	            
	        }
	        
	        EntidadeTemporaria temp;
	        
	        int numEntidades, ind = 0, x = 0, pri = 0, ind2 = 0,  loop = 0;
	       
	        numEntidades = this.et.size();
	        
	        while (numEntidades > 0)
	        {
	            loop++;
	           
	            
	            temp = (filaAux.get(ind)).clone();
	            
	            if (loop == 1)
	            {
	                temp.setTea(0);
	            }
	            
	            int a = temp.getTea()+filaAux.get(ind).getTa();
	            int ind3 = ind;
	            int prioridade = temp.getPri();
	            for (int z=0;z<filaAux.size();z++)
	            {
	                if (prioridade > filaAux.get(z).getPri() && filaAux.get(z).getTc() <= a)
	                {
	                    prioridade = filaAux.get(z).getPri();
	                    ind3 = z;
	                }
	            }
	            
	            if (ind != ind3)
	            {
	               
	                if (loop == 1)
	                {
	                    temp.setTea(0);
	                    temp.setTsa(filaAux.get(ind3).getTc());
	                    temp.setTre((temp.getTea()+temp.getTa())-filaAux.get(ind3).getTc());
	                    temp.setTef(0);
	                   
	                    etAux.add(temp.clone());
	                    
	                    filaAux.get(ind).setTc(temp.clone().getTsa());
	                    filaAux.get(ind).setTea(0);
	                    filaAux.get(ind).setTsa(0);
	                    filaAux.get(ind).setTa(temp.clone().getTre());
	                    filaAux.get(ind).setTre(filaAux.get(ind).getTre());
	                    
	                    ind = ind3;
	                   
	                }
	                else
	                {
	                    
	                    temp.setTea(etAux.get(etAux.size()-1).getTsa());
	                    temp.setTsa(filaAux.get(ind3).getTc());
	                    temp.setTre((temp.getTea()+temp.getTa())-filaAux.get(ind3).getTc());
	                    temp.setTef();
	                    
	                    etAux.add(temp.clone());
	                    
	                    
	                    filaAux.get(ind).setTc(temp.clone().getTsa());
	                    filaAux.get(ind).setTea(0);
	                    filaAux.get(ind).setTsa(0);
	                    filaAux.get(ind).setTa(temp.clone().getTre());
	                    filaAux.get(ind).setTre(filaAux.get(ind).getTre());
	                    
	                    ind = ind3;
	                    
	                }
	            }
	            else
	            {
	               if (loop == 1)
	                {
	                    temp.setTea(0);
	                }
	                else if(filaAux.get(ind).getTc()>etAux.get(etAux.size()-1).getTsa())
	                {
	                    temp.setTea(filaAux.get(ind).getTc()); 
	                    this.taxaOciosidade = this.taxaOciosidade + (temp.getTea() - etAux.get(etAux.size() - 1).getTsa());
	                }
	                else
	                {
	                    temp.setTea(etAux.get(etAux.size()-1).getTsa());
	                    
	                }
	            
	             
		            temp.setTsa(temp.getTea()+temp.getTa());
		            filaAux.get(ind).setTre(0);
		            temp.setTre(0);
		            
		            if (filaAux.get(ind).getTre() == 0)
		            {
		                numEntidades--;
		                filaAux.remove(ind);
		            }
		            
		            temp.setTef();
		            
		            etAux.add(temp.clone());
		           
		            temp = null;
		            
		            if (filaAux.size()==0)
		                    break;
		            
		            ind = 0;
		            ind2 = ind;
		            pri = filaAux.get(ind).getPri();
		            for (x=ind;x<numEntidades;x++)
		            {
		                if(etAux.get(etAux.size()-1).getTsa() >= filaAux.get(x).getTc() && pri > filaAux.get(x).getPri())
		                {
		                    pri = filaAux.get(x).getPri();
		                    ind2 = x;
		                }
		            }
		            if (ind == ind2){
		                ind = 0;
		            }else{
		                ind = ind2;
		            }
	            }
	            //System.out.println("----------> "+etAux.get(ind).getEt());
	        }
	        this.tempoTotaldeExecucao = etAux.get(etAux.size() - 1).getTsa();
	        this.taxaOciosidade = calcula.getTaxaOciosidade(taxaOciosidade, tempoTotaldeExecucao);
	        this.tempoMedioEsperaEmFila = calcula.getMediaTef(etAux);
	        return etAux;
	    }
	
		
}
