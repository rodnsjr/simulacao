package arquivos;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;

import principal.EntidadeTemporaria;


public class Excell {
	
	Workbook wb;
	String arquivoSaida;
    short rowNumber = 2;
    Sheet sheet;
    
    short valAnteriorAtual = 2;
    
    int numExec = 1;
	
	public Excell(String saida)
	{
	    wb = new HSSFWorkbook();
	    //Workbook wb = new XSSFWorkbook();	    
	    this.arquivoSaida = saida;
	}
	
	public void applySaida(ArrayList<EntidadeTemporaria> saida)
	{	    
	    rowNumber++;
	    int init = rowNumber;
	    
	    for (EntidadeTemporaria et : saida) {
			Row myRow = sheet.createRow(rowNumber);
		    myRow.createCell(1).setCellValue((double)et.getEt());
		    myRow.createCell(2).setCellValue((double)et.getPri());
		    myRow.createCell(3).setCellValue((double)et.getTc());
		    myRow.createCell(4).setCellValue((double)et.getTrc());
		    myRow.createCell(5).setCellValue((double)et.getTa());
		    myRow.createCell(6).setCellValue((double)et.getTea());
		    myRow.createCell(7).setCellValue((double)et.getTsa());
		    myRow.createCell(8).setCellValue((double)et.getTef());
		    myRow.createCell(9).setCellValue((double)et.getTre());
		    
		    rowNumber++;
		}
	    //pra dar uma linha de espa�o
	    rowNumber++;
	    
	    int exit = init + saida.size()-1;
	    CellRangeAddress region = new CellRangeAddress(init, exit, 1, 9);
	    RegionUtil.setBorderBottom(CellStyle.BORDER_MEDIUM, region, sheet, wb);
	    RegionUtil.setBorderTop(CellStyle.BORDER_MEDIUM, region, sheet, wb);
	    RegionUtil.setBorderRight(CellStyle.BORDER_MEDIUM, region, sheet, wb);
	    RegionUtil.setBorderLeft(CellStyle.BORDER_MEDIUM, region, sheet, wb);
	    //sheet.addMergedRegion(region);
	    
	}
	
	public void writeHeader(String sheetName, String header)
	{
	    sheet = wb.createSheet(sheetName);
	    Row row = sheet.createRow((short)0);
	    // Create a cell and put a value in it.
	    row.createCell(2).setCellValue(header);
	}
	
	public void writeInfo(String infoName, String info)
	{
		rowNumber += 1;
		Row row = sheet.createRow(rowNumber);
		
		row.createCell(6).setCellValue(infoName);
		row.createCell(9).setCellValue(info);
	}
	
	public void novaExecucao(int numExecucao)
	{
		if (numExec == numExecucao)
		{
			rowNumber = valAnteriorAtual;
		}
		else
			valAnteriorAtual = rowNumber;
		
		this.numExec = numExecucao;
		
	    Row row = sheet.createRow((short)rowNumber);
	    row.createCell(1).setCellValue("Execu��o Numero: ");
	    row.createCell(2).setCellValue(numExecucao);
	    
	    rowNumber += 2;
	    
	    row = null;
	    
	    row = sheet.createRow((short)rowNumber);
	    row.createCell(1).setCellValue("ET");
	    row.createCell(2).setCellValue("PR");
	    row.createCell(3).setCellValue("TC");
	    row.createCell(4).setCellValue("TRC");
	    row.createCell(5).setCellValue("TA");
	    row.createCell(6).setCellValue("TEA");
	    row.createCell(7).setCellValue("TSA");
	    row.createCell(8).setCellValue("TEF");
	    row.createCell(9).setCellValue("TRE");
	}
	
	public void saveFile() throws IOException
	{
	    // Write the output to a file
	    FileOutputStream fileOut = new FileOutputStream(arquivoSaida);
	    wb.write(fileOut);
	    fileOut.close();  
	}
	
	public String getFile()
	{
		return arquivoSaida;
	}
}
