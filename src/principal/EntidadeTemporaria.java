package principal;
import java.util.Comparator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrador
 */
public class EntidadeTemporaria{
    
    /**
     * Temporary Entity, id of the entity
     */
    private int et = 0;
    
    /**
     * Reference Time for Creation
     */
    private int trc = 0;
    
    /**
     * Answer Time
     */
    private int ta = 0;
    
    /**
     * Time of Entry Into Service
    */
    private int tea= 0;
    
    /**
     * Time of Departure Service
    */
    private int tsa= 0;
    
    /**
     * Waiting Time in the Queue
    */
    private int tef = 0 ;
    
    /**
     * Priority
     */
    private int pri = 0;
    
    /**
     * Arrival Time
     */
    private int tc = 0;    
    
    /*
     * Method Constructor
     */
    
    /**
     * Remaining Runtime
     */
    private int tre = 0;
    
    /**
     * 
     * @param et = id
     * @param tc = tempo chegada
     * @param trc = tempo de cria��o
     * @param pri = prioridade
     * @param ta = tempo de atendimento
     */
    public EntidadeTemporaria(int et, int tc, int trc, int pri, int ta) {
        this.et = et;
        this.tc = tc;
        this.trc = trc;
        this.pri = pri;
        this.ta = ta;
        this.tre = this.ta;
        tsa = 0;
        tef = 0;
        tea = 0;
    }    
    
    
    
    public EntidadeTemporaria(int et, int trc, int ta, int tea, int tsa,
			int tef, int pri, int tc, int tre) {
		super();
		this.et = et;
		this.trc = trc;
		this.ta = ta;
		this.tea = tea;
		this.tsa = tsa;
		this.tef = tef;
		this.pri = pri;
		this.tc = tc;
		this.tre = tre;
	}



	/*
     * Methods Setters
     */
    
    public void setTrc(int trc) {
        this.trc = trc;
    }

    public void setTa(int ta) {
        this.ta = ta;
    }

    public void setTea(int tea) {
        this.tea = tea;
    }

    public void setTsa(int tsa) {
        this.tsa = tsa;
    }

    public void setTef() {
        this.tef = this.tea - this.tc;
    }
    
    public void setTef(int value)
    {
    	this.tef = value;
    }
    
    public void esperaEmFila()
    {
    	tef++;
    }

    public void setPri(int pri) {
        this.pri = pri;
    }
    
    public void setTre(int tre) {
        this.tre = tre;
    }
    
    public void setTc(int tc) {
        this.tc = tc;
    }
    
    
    /*
     * Methods Getters
     */
    
    
    public int getEt() {
        return et;
    }
    
    public int getTc() {
        return tc;
    }

    public int getTrc() {
        return trc;
    }

    public int getTa() {
        return ta;
    }

    public int getTea() {
        return tea;
    }

    public int getTsa() {
        return tsa;
    }

    public int getTef() {
        return tef;
    }

    public int getPri() {
        return pri;
    }
    
    public int getTre() {
        return tre;
    }
    
    public String toString()
    {
    	return "ET " + et + " PRI " + pri + " TC " + tc + " TRC " + trc
    			+ " TA " + ta + " TEA " + tea + 
    			" TSA " + tsa + " TEF " + tef + " TRE " + tre;
    }
    
    public EntidadeTemporaria clone()
    {
    	return new EntidadeTemporaria(et,trc,ta,tea,tsa,tef,pri,tc,tre);
    }

    public static Comparator<EntidadeTemporaria> SortByTA
    = new Comparator<EntidadeTemporaria>()
    {
    	public int compare(EntidadeTemporaria et, EntidadeTemporaria et1)
    	{
    		if (et.ta == et1.ta)
    			return 0;
    		else if (et.ta < et1.ta)
    			return -1;
    		else
    			return 1;
    	}    	
    };
    
    public static Comparator<EntidadeTemporaria> SortByTRE
    = new Comparator<EntidadeTemporaria>()
    {
    	public int compare(EntidadeTemporaria et, EntidadeTemporaria et1)
    	{
    		if (et.tre == et1.tre)
    			return 0;
    		else if (et.tre < et1.tre)
    			return -1;
    		else
    			return 1;
    	}
    };
    
    public static Comparator<EntidadeTemporaria> SortByTC
    = new Comparator<EntidadeTemporaria>()
    {
    	public int compare(EntidadeTemporaria et, EntidadeTemporaria et1)
    	{
    		if (et.tc == et1.tc)
    			return 0;
    		else if (et.tc < et1.tc)
    			return -1;
    		else
    			return 1;
    	}
    };
    
    public static Comparator<EntidadeTemporaria> SortBySRT
    = new Comparator<EntidadeTemporaria>()
    {
    	public int compare(EntidadeTemporaria et, EntidadeTemporaria et1)
    	{
    		int tt, tt1;
    		
    		tt = et.tc + et.ta;
    		
    		tt1 = et1.tc + et1.ta;
    		
    		if (tt == tt1)
    			return 0;
    		else if (tt < tt1)
    			return -1;
    		else
    			return 1;
    	}
    };
    
    public static Comparator<EntidadeTemporaria> SortByPri
    = new Comparator<EntidadeTemporaria>() {
    	
    	public int compare(EntidadeTemporaria et, EntidadeTemporaria et1)
    	{
    		if (et.pri == et1.pri)
    			return 0;
    		else if (et.pri < et1.pri)
    			return -1;
    		else
    			return 1;
    	}
	};
	
	public static Comparator<EntidadeTemporaria> SortByLIFO
	= new Comparator<EntidadeTemporaria>()
	{
    	public int compare(EntidadeTemporaria et, EntidadeTemporaria et1)
    	{
    		if (et.tc == et1.tc)
    			return 0;
    		else if (et.tc < et1.tc)
    			return 1;
    		else
    			return -1;
    	}
	};

	public void setEt(int et) {
		// TODO Auto-generated method stub
		this.et = et;
	}
	
    
        
}
