package application;
	
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import controller.MainWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	@Override
	public void start(Stage stage) throws IOException {
		
		
    	InputStream is = new FileInputStream("view/novawindow.fxml");
    	
		FXMLLoader load = new FXMLLoader();
		Parent root = (Parent) load.load(is);
    	
    	is.close();
    	
    	MainWindowController controller = load.getController();
    
        Scene scene = new Scene(root);
    
        stage.setTitle("Escalonamento de Processos");
        stage.setScene(scene);
        
    	controller.initController(stage);
        stage.show();
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
