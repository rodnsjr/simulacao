package escalonamento;

import java.util.ArrayList;

import principal.EntidadeTemporaria;

public class EscFIFO extends Escalonamento {
	
	private Ociosidade_TMEF calcula;

	public EscFIFO(ArrayList<EntidadeTemporaria> et) {
		super(et);
		this.taxaOciosidade = 0;
		this.tempoTotaldeExecucao = 0;
		this.tempoMedioEsperaEmFila = 0;
		this.calcula = new Ociosidade_TMEF();
		// TODO Auto-generated constructor stub
	}

	@Override
	public ArrayList<EntidadeTemporaria> escalonar() {
		// TODO Auto-generated method stub
		ArrayList <EntidadeTemporaria> etAux = new ArrayList<EntidadeTemporaria>();
        EntidadeTemporaria temp;
       
        int i;
        
        for (i=0;i<this.et.size();i++)
        {
            temp = et.get(i);
            if (i == 0)
            {
                temp.setTea(0);                
                temp.setTef();
                temp.setTsa(temp.getTea()+temp.getTa());
                etAux.add(temp);
            }
            else
            {
                if (temp.getTc() > etAux.get(i-1).getTsa())
                {
                    temp.setTea(temp.getTc());
                    this.taxaOciosidade = this.taxaOciosidade + (temp.getTea() - etAux.get(etAux.size() - 1).getTsa());
                }
                else
                {
                    temp.setTea(etAux.get(i-1).getTsa());
                }
                temp.setTef();
                temp.setTsa(temp.getTea()+temp.getTa());
                etAux.add(temp);
            }
        }
        
        for (EntidadeTemporaria entidadeTemporaria : etAux) {
			if (entidadeTemporaria.getTre() > 0)
			{
				entidadeTemporaria.setTre(0);
			}
		}
        
        this.tempoTotaldeExecucao = etAux.get(etAux.size() - 1).getTsa();
        this.taxaOciosidade = calcula.getTaxaOciosidade(taxaOciosidade, tempoTotaldeExecucao);
        this.tempoMedioEsperaEmFila = calcula.getMediaTef(etAux);
        return etAux;
	}
		
}
