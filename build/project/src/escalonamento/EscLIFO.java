package escalonamento;

import java.util.ArrayList;
import java.util.Collections;

import principal.EntidadeTemporaria;

public class EscLIFO extends Escalonamento {

	private ArrayList<EntidadeTemporaria> fila;
	private ArrayList<EntidadeTemporaria> saida;
	

	public EscLIFO(ArrayList<EntidadeTemporaria> et) {
		super(et);
		// TODO Auto-generated constructor stub
		saida = new ArrayList<EntidadeTemporaria>();
		this.taxaOciosidade = 0;
		this.tempoTotaldeExecucao = 0;
		this.tempoMedioEsperaEmFila = 0;
		this.calcula = new Ociosidade_TMEF();
	}

	@Override
	public ArrayList<EntidadeTemporaria> escalonar() {
		ArrayList<EntidadeTemporaria> temp = new ArrayList<EntidadeTemporaria>();
		boolean firstEntry = true;
		
		for (EntidadeTemporaria entidadeTemporaria : et) {
			if (firstEntry)
			{
				firstEntry = false;
				continue;
			}
			
			temp.add(entidadeTemporaria);
		}
		
		Collections.sort(temp, EntidadeTemporaria.SortByTC);		
				
		//gerar TEA, TSA, TEF
		EntidadeTemporaria emAtendimento = et.get(0);
		emAtendimento.setTea(emAtendimento.getTc());
		emAtendimento.setTsa(emAtendimento.getTc() + emAtendimento.getTa());
		emAtendimento.setTef();
		
		fila = new ArrayList<EntidadeTemporaria>();
		
		//pegar o tempo de saida do emAtendimento
		//checar no array quais entidades tem o tempo de chegada menor que esse tempo de saida
		//colocar eles em fila, e escalonar por ordem de atendimento
		//atender o pr�ximo
		while (!temp.isEmpty())
		{	
			EntidadeTemporaria tsaida = null;
			
			//enquanto vai chegando entidades, vai jogando elas na fila
			if (temp.get(0).getTc() <= emAtendimento.getTsa())
			{
				enfila(temp.remove(0));
			}
			//termina de atender a entidade, enfila a atual, e atende a pr�xima
			else
			{
				if (!fila.isEmpty())
				{
					tsaida = fila.remove(0);
					//enfila(temp.remove(0));
				}
				else
				{
					enfila(temp.remove(0));
					tsaida = fila.remove(0);
				}
				
				
				if (tsaida.getTc() <= emAtendimento.getTsa())
				{
					tsaida.setTea(emAtendimento.getTsa());
				}
				else
				{
					tsaida.setTea(tsaida.getTc());
					this.taxaOciosidade = this.taxaOciosidade + (tsaida.getTea() - emAtendimento.getTsa());
				}
				
				tsaida.setTsa(tsaida.getTea() + tsaida.getTa());
				tsaida.setTef();

				this.saida.add(emAtendimento);
				
				emAtendimento = tsaida;
				
			}
			
		}
		
		this.saida.add(emAtendimento);
		
		for (int i = 0; i < fila.size(); i++)
		{
			EntidadeTemporaria tsaida = null;
			tsaida = fila.get(i);
			
			if (tsaida.getTc() <= emAtendimento.getTsa())
			{
				tsaida.setTea(emAtendimento.getTsa());				
			}
			else
			{
				tsaida.setTea(tsaida.getTc());
			}
			
			tsaida.setTsa(tsaida.getTea() + tsaida.getTa());
			tsaida.setTef();
			
			this.saida.add(tsaida);
			
			emAtendimento = tsaida;
		}
		
        for (EntidadeTemporaria entidadeTemporaria : saida) {
			if (entidadeTemporaria.getTre() > 0)
			{
				entidadeTemporaria.setTre(0);
			}
		}
        
        this.tempoTotaldeExecucao = saida.get(saida.size() - 1).getTsa();
        this.taxaOciosidade = calcula.getTaxaOciosidade(taxaOciosidade, tempoTotaldeExecucao);
        this.tempoMedioEsperaEmFila = calcula.getMediaTef(saida);
		return saida;

	}

	private void enfila(EntidadeTemporaria entidadeTemporaria) {
		// TODO Auto-generated method stub
		fila.add(entidadeTemporaria);
		
		if (fila.size() > 1)
			Collections.sort(fila, EntidadeTemporaria.SortByLIFO);
	}
}
