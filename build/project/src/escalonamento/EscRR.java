package escalonamento;

import java.util.ArrayList;

import principal.EntidadeTemporaria;

public class EscRR extends Escalonamento{
	
	
	private int quantum;
	
	public EscRR(ArrayList<EntidadeTemporaria> et) {
		super(et);
		this.taxaOciosidade = 0;
		this.tempoTotaldeExecucao = 0;
		this.tempoMedioEsperaEmFila = 0;
		this.calcula = new Ociosidade_TMEF();
		// TODO Auto-generated constructor stub
	}
	
	public EscRR(ArrayList<EntidadeTemporaria> et, int quantum)
	{
		this(et);
		this.quantum = quantum;
	}

	@Override
	public ArrayList<EntidadeTemporaria> escalonar() {
        ArrayList <EntidadeTemporaria> etAux = new ArrayList<EntidadeTemporaria>();
        //this.resetList();
        ArrayList <EntidadeTemporaria> etEmFila = new ArrayList<EntidadeTemporaria>();
        
        
        
        for (int i = 0; i < this.et.size(); i++) {
            etEmFila.add(this.et.get(i));
            
        }
        
               
        EntidadeTemporaria temp, temp2;
        int i, tempoRestante;
        
        for (i=0;i<etEmFila.size();i++)
        {
            temp = etEmFila.get(i);
            if (i == 0)
            {
                if (temp.getTa() <= quantum)
                {
                    temp.setTea(0);                
                    temp.setTef();
                    temp.setTsa(temp.getTea()+temp.getTa());
                    temp.setTre(0); // adicionado 04/06/2014 08:57
                    etAux.add(temp);
                }
                else
                {
                    temp.setTea(0);                
                    temp.setTef();
                    temp.setTsa(temp.getTea()+quantum);
                    tempoRestante = temp.getTa() - quantum;
                    temp.setTre(tempoRestante);
                    etAux.add(temp);                  
                    
                    
                    temp2 = new EntidadeTemporaria(temp.getEt(), temp.getTsa(), temp.getTrc(), temp.getPri(), tempoRestante);
                    //temp2.setTre(tempoRestante);
                    etEmFila.add(temp2);
                    etEmFila = this.ordenaFila(etEmFila);
                }
            }
            else
            {
                if (temp.getTa() <= quantum)
                {
                    if (temp.getTc() > etEmFila.get(i-1).getTsa())
                    {
                        temp.setTea(temp.getTc());
                        this.taxaOciosidade = this.taxaOciosidade + (temp.getTea() - etAux.get(etAux.size() - 1).getTsa());
                    }
                    else
                    {
                        temp.setTea(etEmFila.get(i-1).getTsa());
                    }
                    temp.setTef();
                    temp.setTsa(temp.getTea()+temp.getTa());
                    temp.setTre(0); // adicionado 04/06/2014 08:57
                    etAux.add(temp);
                }
                else
                {
                    if (temp.getTc() > etEmFila.get(i-1).getTsa())
                    {
                        temp.setTea(temp.getTc());
                        this.taxaOciosidade = this.taxaOciosidade + (temp.getTea() - etAux.get(etAux.size() - 1).getTsa());
                    }
                    else
                    {
                        temp.setTea(etEmFila.get(i-1).getTsa());
                    }
                    temp.setTef();
                    temp.setTsa(temp.getTea()+quantum);
                    tempoRestante = temp.getTa() - quantum;
                    //tempoRestante = 0;
                    temp.setTre(tempoRestante);
                    etAux.add(temp);
                    
                    temp2 = new EntidadeTemporaria(temp.getEt(), temp.getTsa(), temp.getTrc(), temp.getPri(), tempoRestante);
                    etEmFila.add(temp2);
                    
                    etEmFila = this.ordenaFila(etEmFila);
                    
                }
            }
        }
        
        this.tempoTotaldeExecucao = etAux.get(etAux.size() - 1).getTsa();
        this.taxaOciosidade = calcula.getTaxaOciosidade(taxaOciosidade, tempoTotaldeExecucao);
        this.tempoMedioEsperaEmFila = calcula.getMediaTef(etAux);
        return etAux;
	}
	
	private ArrayList <EntidadeTemporaria> ordenaFila (ArrayList<EntidadeTemporaria> entidade)
	{
		EntidadeTemporaria aux;
		for (int i = 0;i<entidade.size();i++)
		{
			for (int j = i;j<entidade.size();j++)
			{
				if (entidade.get(i).getTc() > entidade.get(j).getTc())
				{
					aux = entidade.get(i).clone();
					entidade.set(i, entidade.get(j).clone());
					entidade.set(j, aux);
				}
				else if (entidade.get(i).getTc() == entidade.get(j).getTc())
				{
					if (entidade.get(i).getEt() > entidade.get(j).getEt())
					{
						aux = entidade.get(i).clone();
						entidade.set(i, entidade.get(j).clone());
						entidade.set(j, aux);
					}
				}
			}
		}
		return entidade;
	}
	
}
