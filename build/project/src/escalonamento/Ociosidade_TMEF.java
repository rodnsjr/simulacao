package escalonamento;

import java.util.ArrayList;

import principal.EntidadeTemporaria;

public class Ociosidade_TMEF {
	
	public double getTaxaOciosidade(double taxaOciosidade, double tempoTotaldeExecucao)
	{
		double taxa;
		//System.out.println("taxa = "+taxaOciosidade);
		//System.out.println("tempo = "+tempoTotaldeExecucao);
        taxa =  (taxaOciosidade * 100) / tempoTotaldeExecucao;
        //System.out.println("taxa = "+taxa);
        return taxa;
	}
	
	public double getMediaTef(ArrayList <EntidadeTemporaria> et)
    {
        double  soma = 0;
        for (int i=0;i < et.size();i++)
        {
            soma = soma + et.get(i).getTef();
        }
        
       return  soma / et.size();
       
    }

}
