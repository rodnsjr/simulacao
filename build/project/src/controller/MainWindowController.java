package controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import escalonamento.EscFIFO;
import escalonamento.EscLIFO;
import escalonamento.EscLIFOP;
import escalonamento.EscPRTY;
import escalonamento.EscPRTYP;
import escalonamento.EscRR;
import escalonamento.EscSJF;
import escalonamento.EscSRT;
import escalonamento.Escalonamento;

import principal.*;

import arquivos.Excell;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MainWindowController {

	@FXML
	private TextField textFieldEntidadesSimular, textFieldChegadaX,
			textFieldChegadaY, textFieldTempoSimular, textFieldAtendimentoX,
			textFieldAtendimentoY, textFieldOutput;

	@FXML
	private TextField textFieldQuantum, textFieldPrioridade;

	@FXML
	private TextArea textAreaConsole;

	@FXML
	private Text textSaida, abrindoArquivo;
	
	File arquivoSaida;
	
	Stage st;

	@FXML
	private Button buttonVer;
	
	Excell ex;
	Simulador s;
	int numExecucao = 1;

	public void initController(Stage st) {
		this.st = st;
	}

	@FXML
	protected void onButtonStartClick(MouseEvent event) throws IOException {
		// Pra n�o mostrar 52 milh�es de execu��es no console ...
		clearConsoleClick(null);

		// int numeroSimulacao =
		// Integer.parseInt(textFieldQuantidadeExecucoes.getText());;

		if (ex != null) {
			if (textFieldTempoSimular.getText().equals("0"))
				geraSimulador(false);
			else
				geraSimulador(true);
		} else {
			ex = new Excell("saida.xls");

			if (textFieldTempoSimular.getText().equals("0"))
				geraSimulador(false);
			else
				geraSimulador(true);
		}

		startSimulacao();

		endSimulacao();
	}

	@FXML
	protected void onButtonEscolherClick(MouseEvent event) throws IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Salvar Planilha");
		fileChooser.setInitialDirectory(new File(System
				.getProperty("user.home")));

		fileChooser.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("Planilha", "*.xls"));
		File file = fileChooser.showSaveDialog(st);

		if (file != null) {
			textFieldOutput.setText(file.getAbsoluteFile().toString());
			ex = new Excell(file.getAbsoluteFile().toString() + ".xls");
		}
	}

	private void startSimulacao() throws IOException
	{
		Escalonamento esc;
		
		// Entidades
		ArrayList<EntidadeTemporaria> lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		escreveConsole("Entidades", lista);
		ex.writeHeader("Entidades", "Entidades Geradas");
		ex.novaExecucao(numExecucao);
		ex.applySaida(lista);
		//ex.writeInfo("Tempo de ociosidade", "0");
				
		//Escalonamento FIFO
		lista = null;
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscFIFO(lista);
		ArrayList<EntidadeTemporaria>saida = simula(esc);
		escreveConsole("Escalonamento FIFO", saida);
		ex.writeHeader("FIFO", "Escalonamento FIFO");
		ex.novaExecucao(numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
		//Escalonamento LIFO
		lista = null; saida = null;
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscLIFO(lista);
		saida = simula(esc);
		escreveConsole("Escalonamento LIFO", saida);		
		ex.writeHeader("LIFO", "Escalonamento LIFO");
		ex.novaExecucao(numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
		//Escalonamento LIFO Preemptivo
		lista = null; saida = null;
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscLIFOP(lista);
		saida = simula(esc);
		escreveConsole("Escalonamento LIFO Preemptivo", saida);
		ex.writeHeader("LIFOP", "Escalonamento LIFO Preemptivo");
		ex.novaExecucao(numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
		//Escalonamento RR
		lista = null; saida = null;
		int quantum = Integer.parseInt(textFieldQuantum.getText());
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscRR(lista, quantum);
		saida = simula(esc);
		escreveConsole("Escalonamento Round Robin", saida);
		ex.writeHeader("RR","Escalonamento Round Robin");
		ex.novaExecucao( numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
		
		//Escalonamento PRTY N Preemptivo
		lista = null; saida = null;
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscPRTY(lista);
		saida = simula(esc);
		escreveConsole("Escalonamento Prioridade", saida);
		ex.writeHeader("PRTY","Escalonamento Prioridade");		
		ex.novaExecucao( numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
		//Escalonamento PRTYP
		lista = null; saida = null;
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscPRTYP(lista);
		saida = simula(esc);
		escreveConsole("Escalonamento Prioridade Preemptivo", saida);
		ex.writeHeader("PRTYP", "Escalonamento Prioridade Preemptivo");		
		ex.novaExecucao(numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
		
		//Escalonamento SJF
		lista = null; saida = null;
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscSJF(lista);
		saida = simula(esc);
		escreveConsole("Escalonamento Shortest Job First", saida);
		ex.writeHeader("SJF", "Escalonamento Shortest Job First");
		ex.novaExecucao(numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
		
		//Escalonamento SRT
		lista = null; saida = null;
		lista = (ArrayList<EntidadeTemporaria>) s.getLista();
		esc = new EscSRT(lista);
		saida = simula(esc);
		escreveConsole("Escalonamento Shortest Remaining Time", saida);
		ex.writeHeader("SRT","Escalonamento Shortest Remaining Time");
		ex.novaExecucao( numExecucao);
		ex.applySaida(saida);
		ex.writeInfo("Tempo de ociosidade", ""+ esc.getTaxaOciosidade());
		ex.writeInfo("Tempo m�dio de Espera ", ""+ esc.getTempoMedioEsperaEmFila());
		
	}

	private void endSimulacao() throws IOException {
		ex.saveFile();
		textSaida.setText("Saida gerada Com sucesso no arquivo: " + ex.getFile());
		arquivoSaida = new File(ex.getFile());
		buttonVer.setVisible(true);
		ex = null;
	}

	private ArrayList<EntidadeTemporaria> simula(Escalonamento esc) {
		return s.simular(esc);
	}

	private void geraSimulador(boolean tempo) {
		int numEntidades = Integer
				.parseInt(textFieldEntidadesSimular.getText());
		int chegadaX = Integer.parseInt(textFieldChegadaX.getText());
		int chegadaY = Integer.parseInt(textFieldChegadaY.getText());
		int atendimentoX = Integer.parseInt(textFieldAtendimentoX.getText());
		int atendimentoY = Integer.parseInt(textFieldAtendimentoY.getText());
		int prioridade = Integer.parseInt(textFieldPrioridade.getText());
		int quantum = Integer.parseInt(textFieldQuantum.getText());

		if (!tempo) {
			s = new Simulador(numEntidades, 0, chegadaX, chegadaY,
					atendimentoX, atendimentoY, prioridade, quantum);
			// return s.simular(new EscSJF(s.getLista()));
		} else {
			// int numEntidades =
			// Integer.parseInt(textFieldEntidadesSimular.getText());
			int tempo1 = Integer.parseInt(textFieldTempoSimular.getText());

			s = new Simulador(0, tempo1, chegadaX, chegadaY, atendimentoX,
					atendimentoY, prioridade, quantum);
			// return s.simularPorTempo(new EscSJF(s.getLista()));
		}
	}

	private void escreveConsole(String titulo,
			ArrayList<EntidadeTemporaria> saida) {
		textAreaConsole.setText(textAreaConsole.getText() + titulo + "\n");
		for (EntidadeTemporaria entidadeTemporaria : saida) {
			textAreaConsole.setText(textAreaConsole.getText()
					+ entidadeTemporaria.toString() + "\n");
		}

		textAreaConsole.setText(textAreaConsole.getText() + "\n");
	}

	@FXML
	protected void clearConsoleClick(MouseEvent event) {
		textAreaConsole.setText("");
	}
	
	@FXML
	protected void verClick(MouseEvent event)
	{
		abrindoArquivo.setVisible(true);
		if (arquivoSaida != null)
		{
			try {
				Desktop.getDesktop().open(arquivoSaida);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
