package principal;

import java.util.ArrayList;
import java.util.Random;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrador
 */
public class Simulacao {
    
    /**
     * Número de Entidades a Simular
     */
    protected int numEntidades;
    
    /**
     * Tempo de Simulação
     */
    protected int tempoSimulacao;
    
    /**
     * Valor inicial do intervalo que será
     * sorteado para o tempo de chegada
     */
    protected int tempoChegadaX;
    
    /**
     * Valor final do intervalo que será
     * sorteado para o tempo de chegada
     */
    protected int tempoChegadaY;
    
    /**
     * Valor inicial do intervalo que será
     * sorteado para o tempo de atendimento
     */
    protected int tempoAtendimentoX;
    
    /**
     * Valor final do intervalo que será
     * sorteado para o tempo de atendimento
     */
    protected int tempoAtendimentoY;
    
    /**
     * Valor final do intervalo que será
     * sorteado para o intervalo de prioridade
     */
    protected int limitePrioridade;
    
    /**
     * Quantum do simulador
     */
    protected int quantum;
    
    /**
     * Entidades
     */
    protected ArrayList <EntidadeTemporaria> et = new ArrayList<EntidadeTemporaria>();
    
    /*
     * Método construtor
     */
    public Simulacao(int numEntidades, int tempoSimulacao, int tempoChegadaX, 
            int tempoChegadaY, int tempoAtendimentoX, int tempoAtendimentoY, 
            int limitePrioridade, int quantum) {
        
        this.numEntidades = numEntidades;
        this.tempoSimulacao = tempoSimulacao;
        this.tempoChegadaX = tempoChegadaX;
        this.tempoChegadaY = tempoChegadaY;
        this.tempoAtendimentoX = tempoAtendimentoX;
        this.tempoAtendimentoY = tempoAtendimentoY;
        this.limitePrioridade = limitePrioridade;
        this.quantum = quantum;
        
        //this.setChegadaEntidades();
        //this.simular();
        
    }
    
    /*
     * Métodos getters
     */
    public int getNumEntidades() {
        return numEntidades;
    }

    public int getTempoSimulacao() {
        return tempoSimulacao;
    }

    public int getTempoChegadaX() {
        return tempoChegadaX;
    }

    public int getTempoChegadaY() {
        return tempoChegadaY;
    }

    public int getTempoAtendimentoX() {
        return tempoAtendimentoX;
    }

    public int getTempoAtendimentoY() {
        return tempoAtendimentoY;
    }

    public int getLimitePrioridade() {
        return limitePrioridade;
    }

    public int getQuantum() {
        return quantum;
    }
    
    /*
     * Funcionalidades
     */
    
    public void simular()
    {
        //this.escalonador = new Escalonamento(et);
        
    }
    
    private int sortTmpAtend()
    {
        Random r = new Random();
        return (r.nextInt((this.tempoAtendimentoY-this.tempoAtendimentoX)+1)+this.tempoAtendimentoX);
        
    }
    
    private int sortTmpChegada() 
    {
        Random r = new Random();
        return (r.nextInt((this.tempoChegadaY-this.tempoChegadaX)+1)+this.tempoChegadaX);
        
    }
    
    private int sortPrioridade() 
    {
        Random r = new Random();
        return r.nextInt(this.limitePrioridade)+1;
        
    }
    
    protected void setChegadaEntidades()
    {
        int i = 0, tmpAux, tmpChegada;
        EntidadeTemporaria aux;
        
        aux = new EntidadeTemporaria(i+1, i, i, this.sortPrioridade(), 
                this.sortTmpAtend());
        this.et.add(aux);
        if (numEntidades > 0)
        {
            for (i=i+1;i<numEntidades;i++)
            {
               // System.out.println("i = "+i);
                tmpAux = this.sortTmpChegada();
                tmpChegada = this.et.get(i-1).getTc() + tmpAux;
                aux = new EntidadeTemporaria(i+1, tmpChegada, tmpAux, 
                        this.sortPrioridade(), this.sortTmpAtend());
                this.et.add(aux);
            }
        }
        else
        {
            for(i=i+1;;i++)
            {
                tmpAux = this.sortTmpChegada();
                tmpChegada = this.et.get(i-1).getTc() + tmpAux;
                
                if (tmpChegada > this.tempoSimulacao)
                    break;
                
                aux = new EntidadeTemporaria(i+1, tmpChegada, tmpAux,
                        this.sortPrioridade(), this.sortTmpAtend());
                this.et.add(aux);
            }
        }
        /*
        for (i=0;i<this.et.size();i++)
        {
            System.out.println("Et = "+this.et.get(i).getEt()+" TC = "
                    +this.et.get(i).getTc()+" TRC = "+this.et.get(i).getTrc()
                    +" Pri = "+this.et.get(i).getPri()+" TA = "+this.et.get(i).getTa());
        }
        System.out.println("\n\n");*/
    }
    
}
