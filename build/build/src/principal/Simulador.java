package principal;

import java.util.ArrayList;
import java.util.Random;

import escalonamento.Escalonamento;

public class Simulador extends Simulacao{
	
	public Simulador(int numEntidades, int tempoSimulacao, int tempoChegadaX,
			int tempoChegadaY, int tempoAtendimentoX, int tempoAtendimentoY,
			int limitePrioridade, int quantum) {
		super(numEntidades, tempoSimulacao, tempoChegadaX, tempoChegadaY,
				tempoAtendimentoX, tempoAtendimentoY, limitePrioridade, quantum);
		//System.out.println("numEntidades: " + numEntidades);
		setChegadaEntidades();
		//System.out.println(this.et.toString());
	}

	//private ArrayList<EntidadeTemporaria> et = new ArrayList<EntidadeTemporaria>();
	
	//private int entidades, chegadaX, chegadaY, atendimentoX, atendimentoY;
	
	public boolean porTempo;

	
	protected void initSimulador()
	{
		// TODO Auto-generated method stub

		
		//id, tempo de chegada, tempo de cria��o, prioridade, tempo de atendimento
		Random r = new Random();
		//int chegada = r.nextInt(chegadaY) + chegadaX;
		int atendimento = r.nextInt(tempoAtendimentoY) + tempoAtendimentoX;
		
		int prioridade = 1;
		
		
		
		et.add(new EntidadeTemporaria(0, 0, 0, prioridade, atendimento));
		/*
		et.add(new EntidadeTemporaria(1, 3, 3, 1, 1));
		et.add(new EntidadeTemporaria(2, 1, 1, 1, 2));
		et.add(new EntidadeTemporaria(3, 2, 2, 1, 5));
		*/
		
		
		
		for (int i = 1; i < numEntidades; i++)
		{
			int chegada1 = r.nextInt(tempoChegadaY) + tempoChegadaX;
			int atendimento1 = r.nextInt(tempoAtendimentoY) + tempoAtendimentoX;
			et.add(new EntidadeTemporaria(i, chegada1, chegada1, prioridade, atendimento1));
		}
		
	}

	public ArrayList<EntidadeTemporaria> getLista()
	{
		ArrayList<EntidadeTemporaria> lista = new ArrayList<EntidadeTemporaria>();
		
		for (EntidadeTemporaria entidadeTemporaria : et) {
			lista.add(entidadeTemporaria.clone());
		}
		
		return lista;
	}
	
	public ArrayList<EntidadeTemporaria> simular(Escalonamento esc)
	{		
		
		//EscSJF esc = new EscSJF(et);
		//EscSRT esc = new EscSRT(et);
		//EscPRTY esc = new EscPRTY(et);
		
		ArrayList<EntidadeTemporaria> saida;
		saida = esc.escalonar();
		
		/*
		for (EntidadeTemporaria entidadeTemporaria : saida) {
			System.out.println(entidadeTemporaria.toString());
		}*/
		
		return saida;
	}
}
