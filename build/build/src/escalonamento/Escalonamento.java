package escalonamento;

import java.util.ArrayList;

import principal.EntidadeTemporaria;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrador
 */
public abstract class Escalonamento {
    
    /**
     * Entidades para realizar a simulação
     * Já possuem os valores de ET, TC, TRC, PRI e TEA 
     */
    protected ArrayList <EntidadeTemporaria> et = new ArrayList<EntidadeTemporaria>();
    
    protected double taxaOciosidade, tempoMedioEsperaEmFila;
    protected double tempoTotaldeExecucao;
    protected Ociosidade_TMEF calcula;

    public abstract ArrayList<EntidadeTemporaria> escalonar();
    /*
     * Método
     * Construtor
     */
    public Escalonamento(ArrayList<EntidadeTemporaria> et) 
    {
        this.et = et;
    }
    
	public double getTaxaOciosidade() {
		return taxaOciosidade;
	}

	public double getTempoMedioEsperaEmFila() {
		return tempoMedioEsperaEmFila;
	}
    
}
