package escalonamento;
import java.util.ArrayList;

import principal.EntidadeTemporaria;


public class EscPRTY extends Escalonamento{

	ArrayList<EntidadeTemporaria> saida = new ArrayList<EntidadeTemporaria>();
		
	public EscPRTY(ArrayList<EntidadeTemporaria> et) {
		super(et);
		this.taxaOciosidade = 0;
		this.tempoTotaldeExecucao = 0;
		this.tempoMedioEsperaEmFila = 0;
		this.calcula = new Ociosidade_TMEF();
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<EntidadeTemporaria> escalonar()
	{
        ArrayList <EntidadeTemporaria> etAux = new ArrayList<EntidadeTemporaria>();
        //this.resetList();
        ArrayList <EntidadeTemporaria> filaAux = new ArrayList<EntidadeTemporaria>();
        
        for (int i = 0; i < this.et.size(); i++) {
            filaAux.add(this.et.get(i));
            
        }
        
        EntidadeTemporaria temp;
        
        int numEntidades, ind = 0, x = 0, pri = 0, ind2 = 0;
       
        numEntidades = this.et.size();
        
        while (numEntidades > 0)
        {
            
            temp = filaAux.get(ind);
            
            if (temp.getEt() == 1)
            {
                temp.setTea(0);
            }
            else if(filaAux.get(ind).getTc()>etAux.get(etAux.size()-1).getTsa())
            {
                temp.setTea(filaAux.get(ind).getTc()); 
                this.taxaOciosidade = this.taxaOciosidade + (temp.getTea() - etAux.get(etAux.size() - 1).getTsa());
            }
            else
            {
                temp.setTea(etAux.get(etAux.size()-1).getTsa());
                //filaAux.get(ind).
            }
            
            
            temp.setTsa(temp.getTea()+filaAux.get(ind).getTre());
            filaAux.get(ind).setTre(0);
            
            if (filaAux.get(ind).getTre() == 0)
            {
                numEntidades--;
                filaAux.remove(ind);
            }
            
            temp.setTef();
            etAux.add(temp);
            
            if (filaAux.size()==0)
                    break;
            
            ind = 0;
            ind2 = ind;
            pri = filaAux.get(ind).getPri();
            for (x=ind;x<numEntidades;x++)
            {
                if(temp.getTsa() >= filaAux.get(x).getTc() && pri > filaAux.get(x).getPri())
                {
                    pri = filaAux.get(x).getPri();
                    ind2 = x;
                }
            }
            if (ind == ind2){
                ind = 0;
            }else{
                ind = ind2;
            }
            
            //System.out.println("----------> "+etAux.get(ind).getEt());
        }
        
        this.tempoTotaldeExecucao = etAux.get(etAux.size() - 1).getTsa();
        this.taxaOciosidade = calcula.getTaxaOciosidade(taxaOciosidade, tempoTotaldeExecucao);
        this.tempoMedioEsperaEmFila = calcula.getMediaTef(etAux);
        return etAux;
	}
	
}
